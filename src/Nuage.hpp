#ifndef NUAGE_HPP
#define NUAGE_HPP

#include <vector>
#include "Point.hpp"
#include "Cartesien.hpp"
#include "Polaire.hpp"

class Nuage {
public:
  Nuage();
  
	inline int size() {return _nuage.size();};
	void ajouter(Cartesien c);
	void ajouter(Polaire p);
  
private:
  std::vector<Point*> _nuage;
};

#endif
