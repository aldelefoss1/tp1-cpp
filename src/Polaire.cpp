#include "Polaire.hpp"
#include "Cartesien.hpp"
#include <cmath>

Polaire::Polaire() {
	_angle = 0;
	_distance = 0;
}

Polaire::Polaire(double angle, double distance) {
	_angle = angle;
	_distance = distance;
}

Polaire::Polaire(Cartesien c) {
  c.convertir(*this);
}

void Polaire::afficher(std::ostream& flux) const {
	flux << "(a=" << _angle << ";d=" << _distance << ")";
}

void Polaire::convertir(Cartesien c) const {
  c.setX(_distance * std::cos(_angle));
  c.setY(_distance * std::sin(_angle));
}