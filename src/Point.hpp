#ifndef POINT_HPP
#define POINT_HPP

#include <iostream>
class Cartesien;
class Polaire;

class Point{
	public :
	  virtual void convertir(Polaire& p) const;
	  virtual void convertir(Cartesien& c) const;
	  virtual void afficher(std::ostream& flux) const = 0;
};

std::ostream &operator<<(std::ostream &flux, Point const& p);

#endif