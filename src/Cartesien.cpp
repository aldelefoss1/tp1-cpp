#include "Cartesien.hpp"
#include "Polaire.hpp"
#include <cmath>

Cartesien::Cartesien() {
	_x = 0;
	_y = 0;
}

Cartesien::Cartesien(double x, double y) {
	_x = x;
	_y = y;
}

Cartesien::Cartesien(Polaire p) {
  p.convertir(*this);
}

void Cartesien::afficher(std::ostream& flux) const{
	flux << "(x=" << _x << ";y=" << _y << ")";
}

void Cartesien::convertir(Polaire p) const {
  p.setDistance(std::sqrt(_x*_x + _y*_y));
  p.setAngle(atan(_y/_x));
}