#ifndef CART_HPP
#define CART_HPP

#include "Point.hpp"

class Polaire;

class Cartesien : public Point {
public:
	Cartesien();
	Cartesien(double, double);
	Cartesien(Polaire p);

	inline void setX(double x) { _x = x; };
	inline void setY(double y) { _y = y; };
	inline double getX() const { return _x; };
	inline double getY() const { return _y; };
	
	void afficher(std::ostream& flux) const;
	void convertir(Polaire p) const;
	
private:
	double _x;
	double _y;
};
#endif
