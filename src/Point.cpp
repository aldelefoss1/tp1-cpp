#include "Point.hpp"
#include "Cartesien.hpp"
#include "Polaire.hpp"

void Point::convertir(Polaire& p) const {}
void Point::convertir(Cartesien& c) const {}

std::ostream &operator<<(std::ostream &flux, Point const& p)
{
  p.afficher(flux);
  return flux;
}