#ifndef POLAIRE_HPP
#define POLAIRE_HPP

#include "Point.hpp"

class Cartesien;

class Polaire : public Point {
public:
	Polaire();
	Polaire(double, double);
	Polaire(Cartesien c);

	inline void setAngle(double angle) { _angle = angle; };
	inline void setDistance(double distance) { _distance = distance; };
	inline double getAngle() const { return _angle; };
	inline double getDistance() const { return _distance; };
	
	void afficher (std::ostream& flux) const;
	void convertir(Cartesien c) const;
	
private:
	double _angle;
	double _distance;
};

#endif
